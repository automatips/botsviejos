﻿namespace ultraFaceW32
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbxUsuario = new System.Windows.Forms.GroupBox();
            this.tbxUsuario = new System.Windows.Forms.TextBox();
            this.gbxContraseña = new System.Windows.Forms.GroupBox();
            this.tbxContraseña = new System.Windows.Forms.TextBox();
            this.btnIniciarCampaña = new System.Windows.Forms.Button();
            this.gbxProgreso = new System.Windows.Forms.GroupBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblStatus = new System.Windows.Forms.Label();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.timerReboot = new System.Windows.Forms.Timer(this.components);
            this.gbxUsuario.SuspendLayout();
            this.gbxContraseña.SuspendLayout();
            this.gbxProgreso.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbxUsuario
            // 
            this.gbxUsuario.Controls.Add(this.tbxUsuario);
            this.gbxUsuario.Location = new System.Drawing.Point(13, 13);
            this.gbxUsuario.Name = "gbxUsuario";
            this.gbxUsuario.Size = new System.Drawing.Size(227, 47);
            this.gbxUsuario.TabIndex = 0;
            this.gbxUsuario.TabStop = false;
            this.gbxUsuario.Text = "Usuario";
            // 
            // tbxUsuario
            // 
            this.tbxUsuario.Location = new System.Drawing.Point(7, 20);
            this.tbxUsuario.Name = "tbxUsuario";
            this.tbxUsuario.Size = new System.Drawing.Size(214, 20);
            this.tbxUsuario.TabIndex = 0;
            this.tbxUsuario.Text = "karim090";
            // 
            // gbxContraseña
            // 
            this.gbxContraseña.Controls.Add(this.tbxContraseña);
            this.gbxContraseña.Location = new System.Drawing.Point(246, 13);
            this.gbxContraseña.Name = "gbxContraseña";
            this.gbxContraseña.Size = new System.Drawing.Size(227, 47);
            this.gbxContraseña.TabIndex = 1;
            this.gbxContraseña.TabStop = false;
            this.gbxContraseña.Text = "Contraseña";
            // 
            // tbxContraseña
            // 
            this.tbxContraseña.Location = new System.Drawing.Point(7, 20);
            this.tbxContraseña.Name = "tbxContraseña";
            this.tbxContraseña.PasswordChar = '*';
            this.tbxContraseña.Size = new System.Drawing.Size(214, 20);
            this.tbxContraseña.TabIndex = 0;
            this.tbxContraseña.Text = "123456";
            // 
            // btnIniciarCampaña
            // 
            this.btnIniciarCampaña.Location = new System.Drawing.Point(12, 66);
            this.btnIniciarCampaña.Name = "btnIniciarCampaña";
            this.btnIniciarCampaña.Size = new System.Drawing.Size(461, 37);
            this.btnIniciarCampaña.TabIndex = 2;
            this.btnIniciarCampaña.Text = "INICIAR CAMPAÑA";
            this.btnIniciarCampaña.UseVisualStyleBackColor = true;
            this.btnIniciarCampaña.Click += new System.EventHandler(this.btnIniciarCampaña_Click);
            // 
            // gbxProgreso
            // 
            this.gbxProgreso.Controls.Add(this.progressBar1);
            this.gbxProgreso.Location = new System.Drawing.Point(13, 109);
            this.gbxProgreso.Name = "gbxProgreso";
            this.gbxProgreso.Size = new System.Drawing.Size(460, 45);
            this.gbxProgreso.TabIndex = 3;
            this.gbxProgreso.TabStop = false;
            this.gbxProgreso.Text = "Progreso actual: 0%";
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(6, 16);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(448, 23);
            this.progressBar1.TabIndex = 0;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(479, 13);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(240, 135);
            this.webBrowser1.TabIndex = 4;
            this.webBrowser1.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(17, 167);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 13);
            this.lblStatus.TabIndex = 5;
            // 
            // timer2
            // 
            this.timer2.Interval = 10000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // timerReboot
            // 
            this.timerReboot.Interval = 2800000;
            this.timerReboot.Tick += new System.EventHandler(this.timerReboot_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 190);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.gbxProgreso);
            this.Controls.Add(this.btnIniciarCampaña);
            this.Controls.Add(this.gbxContraseña);
            this.Controls.Add(this.gbxUsuario);
            this.Name = "Form1";
            this.Opacity = 0.95;
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Iniciador de campaña";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.gbxUsuario.ResumeLayout(false);
            this.gbxUsuario.PerformLayout();
            this.gbxContraseña.ResumeLayout(false);
            this.gbxContraseña.PerformLayout();
            this.gbxProgreso.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbxUsuario;
        private System.Windows.Forms.TextBox tbxUsuario;
        private System.Windows.Forms.GroupBox gbxContraseña;
        private System.Windows.Forms.TextBox tbxContraseña;
        private System.Windows.Forms.Button btnIniciarCampaña;
        private System.Windows.Forms.GroupBox gbxProgreso;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Timer timerReboot;
    }
}

