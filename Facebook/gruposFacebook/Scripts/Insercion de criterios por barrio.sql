insert into criterio(nombre,ultimaEjecucion,activo,idUser)
select distinct concat(c.nombre,' en ', b.nombre), DATE_ADD(current_timestamp(), INTERVAL -1 DAY),1,1
from criterio c, controlmundial.barrio b
where c.idUser = 1
and c.activo = 1;