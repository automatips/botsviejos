﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication3
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string v_Usuario = Convert.ToString(Request.QueryString["usuario"]);
            string v_id = Convert.ToString(Request.QueryString["identificador"]);
            Label1.Text = "Bienvenido " + v_Usuario + " identificador " + v_id;
        }
    }
}