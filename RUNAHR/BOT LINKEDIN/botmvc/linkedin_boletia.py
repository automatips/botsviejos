from datos.LinkedinDatos import ObtenerCuentaLinkedin
from entidades.Bot import Bot
from entidades.Linkedin import Linkedin
from negocio.BotNegocio import iniciar_navegador
from negocio.LinkedinNegocio import *



def main():

    linkedin = Linkedin()

    usuario, contrasena, cantidad_criterios = ObtenerCuentaLinkedin()
    linkedin.usuario = usuario
    linkedin.contrasena = contrasena

    bot = Bot(linkedin)
    iniciar_navegador(bot)
    iniciar_sesion(bot)
    for a in range(cantidad_criterios):
        buscar_perfil(bot)

