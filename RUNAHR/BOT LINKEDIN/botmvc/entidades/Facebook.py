



class Facebook():
    usuario = None
    contrasena = None
    url = "https://www.facebook.com"
    url_administracion_grupos = "https://www.facebook.com/groups/?ref=bookmarks"

    #ventana login
    XPATH_campo_correo_login = ".//input[@name='email']"
    XPATH_campo_contrasena = ".//input[@name='pass']"
    XPATH_control_sesion_iniciada = ".//div[text()='Configuración de la cuenta']"

    #barra menu superior
    XPATH_boton_inicio = ".//a[text()='Inicio']"
    XPATH_boton_fotito_propia = ".//a[@title='Perfil']"
    XPATH_boton_flechita_menu = ".//div[text()='Configuración de la cuenta']"
    XPATH_boton_cerrar_sesion = ".//span[text()='Salir']"
    XPATH_campo_busqueda = ".//div[@role = 'banner']//input[@placeholder = 'Buscar']"

    #pantalla buscador
    XPATH_boton_mas = ".//span[text() = 'Más']"
    XPATH_boton_eventos = ".//a[@data-edge = 'keywords_events']"
    XPATH_boton_paginas = ".//li[@data-edge = 'keywords_pages']/a"


    #resultados paginas

    XPATH_lista_contenedores_resultado = ".//div[@id = 'pagelet_loader_initial_browse_result']/div/div/div"
    XPATH_resultados = "./span"
    XPATH_resultados_alt = "./div/span"
    XPATH_resultado_contenido1 = "./div/div/div/div"


class Pagina():
    url = None
    fbid = None
    nombre = None
    linea1 = None
    linea2 = None
    def __init__(self, _url, _nombre, _linea1, _linea2):
        self.url = _url
        self.nombre = _nombre
        self.linea1 = _linea1
        self.linea2 = _linea2












