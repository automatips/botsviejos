
class Linkedin():

    usuario = None
    contrasena = None
    url = "https://www.linkedin.com"
    leads = []

    idioma = None
    # barra superior
    XPATH_campo_busqueda = ".//input[@placeholder = 'Búsqueda']"

    # con sesion cerrada
    XPATH_campo_correo_login = ".//input[@placeholder = 'Correo electrónico']"
    XPATH_campo_correo_login_ing = ".//input[@placeholder = 'Email']"
    XPATH_campo_contrasena = ".//input[@placeholder = 'Contraseña']"
    XPATH_campo_contrasena_ing = ".//input[@placeholder = 'Password']"
    XPATH_boton_iniciar_sesion = ".//input[@value = 'Inicia sesión']"
    XPATH_boton_iniciar_sesion_ing = ".//input[@value = 'Sign in']"
    XPATH_control_sesion_iniciada = ".//button[@data-control-name = 'nav.settings']"

    # resultado de busqueda
    # XPATH_resultado_de_busqueda = ".//li[@class = 'search-result search-result__occluded-item ember-view']"
    XPATH_resultado_de_busqueda = ".//li[contains(@class,'search-result search-result__occluded-item')]"
    XPATH_resultado_de_busqueda_oculto = ".//li[@class = 'search-result search-result__occluded-item search-result__occlusion-hint ember-view']"
    XPATH_resultado_de_busqueda_borroso = ".//li[@class = 'search-result search-result__occluded-item ember-view']"
    XPATH_titulo_resultado_busqueda = ".//span"
    # XPATH_titulo_resultado_busqueda = ".//span[contains(@class,'actor-name')]"
    XPATH_sublinea1_resultado_busqueda = ".//p[@class = 'subline-level-1 Sans-15px-black-85% search-result__truncate']"
    XPATH_sublinea2_resultado_busqueda = ".//p[@class = 'subline-level-2 Sans-13px-black-55% search-result__truncate']"
    XPATH_sublinea3_resultado_busqueda = ".//p[@class = 'search-result__snippets mt2 Sans-13px-black-55%']"
    XPATH_boton_conectar = ".//button[text() = 'Conectar']"
    # paginador
    XPATH_boton_pagina_siguiente = ".//button[@class = 'next']"
    XPATH_pagiandor_completo = ".//li[@class = 'page-list']"

    # ventana perfil miembro
    XPATH_boton_ver_informacion = ".//a[@data-control-name = 'contact_see_more']"
    XPATH_boton_trabajo_actual = ".//button[@class = 'pv-top-card-v2-section__link pv-top-card-v2-section__link-experience mb1']"
    XPATH_nombre_perfil = ".//h1[@class = 'pv-top-card-section__name inline Sans-26px-black-85%']"
    XPATH_puesto_perfil = ".//h2[@class = 'pv-top-card-section__headline mt1 Sans-19px-black-85%']"
    XPATH_ciudad_pais_perfil = ".//h3[@class = 'pv-top-card-section__location Sans-17px-black-55%-dense mt1 inline-block']"



    def __init__(self):


        pass

class Lead():
    nombre_ultima_compania = None
    sitio_compania = None
    nombre = None
    apellido = None
    email = None
    puesto = None
    ciudad = None
    pais = None
    telefono = None
    industria = None
    rango = None
    url = None
    def __init__(self, nombre_ultima_compania, sitio_compania, nombre, apellido, email, puesto, ciudad, pais, telefono, industria, rango, url):
        self.nombre_ultima_compania = nombre_ultima_compania
        self.sitio_compania = sitio_compania
        self.nombre = nombre
        self.apellido = apellido
        self.email = email
        self.puesto = puesto
        self.ciudad = ciudad
        self.pais = pais
        self.telefono = telefono
        self.industria = industria
        self.rango = rango
        self.url = url
